const Parser = require("./ParserClassNode")
const fs = require("fs")
const args = process.argv;
args.splice(0, 1);
args.splice(0, 1);

args.forEach(a => {
    if(fs.existsSync(process.cwd() + "/" + a))
    {
        var p = new Parser(fs.readFileSync(`${process.cwd()}/${a}`, "utf-8"))
        p.parse();
        process.exit(0);
    }

    if(a == args[args.length-1])
    {    
        console.error("Could not find any file!")
        process.exit(1)
    }
})