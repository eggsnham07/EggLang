class EggLangParser {
    instructions = null;
    type = "system";
    lineInt = 0;
    loaded = [];
    /**
     * 
     * @param {String} file 
     */
    constructor(file)
    {
        if(file) this.instructions = file.trim();
    }

    async parse(infile=this.instructions)
    {
        return new Promise(async (resolve, reject) => {
            for(const line of infile.split("\n"))
            {
                this.lineInt++;
                for(const ln of line.split(";"))
                {
                    try {
                        const r = await this.read(ln);
                        if(r != "OK") throw new Error(r);
                        resolve();
                    } catch(e) {
                        reject(e);
                    }
                }
            }
        })
    }

    async read(ln)
    {
        ln = ln.replace(/\t/g, '');
        if(ln == "") return "OK";
        if(ln.startsWith("//")) return "OK";
        return new Promise(async (resolve, reject) => {
            const instruction = ln.split(" ")[0];
            const params = ln.split(" ");
            params.splice(0, 1);
            if(this.type.includes("::html")) {
                if(!typeof parse_instruction == "function") throw new Error("Instruction parser library has not been loaded yet!")
                try {
                    var r = await parse_instruction(instruction, params, this);
                    resolve(r);
                    return;
                } catch(e) {
                    reject(e);
                    return;
                }
            }

            switch(instruction)
            {
                case "print":
                case "echo":
                    if(params.length == 0) reject(`${instruction} requires 1+ arguments but got none`)
                    if(params.join(" ").match(/"(.*)"/))
                    {
                        console.log(params.join(" ").replace(/"(.*)"/, "$1"));
                    }
                    else
                    {
                        console.log(params[0]);
                    }
                    break;
                case "clear":
                    console.clear();
                    break;
                default:
                    console.log(`[ERROR]: '${instruction}' is undefined`);
                    break;
            }
        })
    }
}


module.exports = EggLangParser;